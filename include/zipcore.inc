#if defined _zipcore_included
 #endinput
#endif
#define _zipcore_included

#include <zipcore/client>
#include <zipcore/effect>
#include <zipcore/file>
#include <zipcore/menu>
#include <zipcore/trace>
#include <zipcore/vector>