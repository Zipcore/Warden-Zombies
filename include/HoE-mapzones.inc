#if defined _HoE_mapzones_included
#endinput
#endif
#define _HoE_mapzones_included

enum MapZoneType
{
	iStart,
	iEnd
}

native void HOE_Mapzones_SkipNextOutput(int client, bool bSkip = true);
native bool HOE_Mapzones_IsOutputBlocked(int client);
native bool HOE_Mapzones_DoesTimerExist();

native int HOE_Mapzones_GetPlayerZoneID(int client);
native int HOE_Mapzones_GetPlayerCourseID(int client);

native void HOE_Mapzones_GetZoneName(int ZoneID, char[] sNameBuffer, int iLength);
native MapZoneType HOE_Mapzones_GetZoneType(int ZoneID);

native int HOE_Mapzones_GetCourseID(int CourseID);
native void HOE_Mapzones_GetCourseName(int CourseID, char[] sNameBuffer, int iLength);
native int HOE_Mapzones_GetCoursePoints(int CourseID);
native int HOE_Mapzones_GetCoursesCount();
native void HOE_Mapzones_ResetCourseID(int client);

native bool HOE_Mapzones_PluginStatus();

#if defined REQUIRE_PLUGIN
public __pl_HoE_mapzones_SetNTVOptional()
{
	MarkNativeAsOptional("HOE_Mapzones_SkipNextOutput");
	MarkNativeAsOptional("HOE_Mapzones_IsOutputBlocked");
	MarkNativeAsOptional("HOE_Mapzones_DoesTimerExist");
	
	MarkNativeAsOptional("HOE_Mapzones_GetPlayerZoneID");
	MarkNativeAsOptional("HOE_Mapzones_GetPlayerCourseID");
	
	MarkNativeAsOptional("HOE_Mapzones_GetZoneName");
	MarkNativeAsOptional("HOE_Mapzones_GetZoneType");
	
	MarkNativeAsOptional("HOE_Mapzones_GetCourseID");
	MarkNativeAsOptional("HOE_Mapzones_GetCourseName");
	MarkNativeAsOptional("HOE_Mapzones_GetCoursePoints");
	MarkNativeAsOptional("HOE_Mapzones_GetCoursesCount");
	MarkNativeAsOptional("HOE_Mapzones_ResetCourseID");
	
	MarkNativeAsOptional("HOE_Mapzones_PluginStatus");
}
#endif

public SharedPlugin __pl_HoE_mapzones =
{
	name = "HoE-mapzones",
	file = "HoE-mapzones.smx",
#if defined REQUIRE_PLUGIN
	required = 1,
#else
	required = 0,
#endif
};
