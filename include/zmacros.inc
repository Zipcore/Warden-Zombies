#if defined _zmacros_included
 #endinput
#endif
#define _zmacros_included

//Python + C-style coperators
#define and			&&
#define and_eq		&=
#define bitand		&
#define bitor		|
#define compl		~
#define not			!
#define not_eq		!=
#define or			||
#define or_eq		|=
#define xor			^
#define xor_eq		^=
#define bitl		<<
#define bitr		>>
#define eq			==

#define addeq		+=
#define subeq		-=
#define multeq		*=
#define diveq		/=
#define mod			%
#define modeq		%=

#define LoopConnectedClients(%1) for(int %1=1;%1<=MaxClients;++%1)\
								if(IsClientConnected(%1))
							
#define LoopIngameClients(%1) for(int %1=1;%1<=MaxClients;++%1)\
								if(IsClientInGame(%1))
							
#define LoopIngamePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
								if(IsClientInGame(%1) && !IsFakeClient(%1))
							
#define LoopAuthorizedPlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
								if(IsClientConnected(%1) && IsClientAuthorized(%1))
							
#define LoopAlivePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
								if(IsClientInGame(%1) && IsPlayerAlive(%1))
								
//functional-style typecasting
#define int(%1)		view_as<int>(%1)
#define float(%1)	view_as<float>(%1)
#define bool(%1)	view_as<bool>(%1)
#define cast-%1(%2)	view_as<%1>(%2) // example -> TFClassType type = cast-TFClassType(1) returns the integer as TFClass_Scout
#define view-%1(%2)	view_as<%1>(%2)

//misc.
#define string		char[]
#define str(%1)		char %1[] //use like 'string(szString) = "this is a string"';
#define del		delete
#define nullfunc	INVALID_FUNCTION
#define nullvec		NULL_VECTOR
#define nullstr		NULL_STRING
#define toggle(%1)	%1 = not %1
#define tern(%1:%2)	? %1 : %2
#define max(%1, %2)	(%1 > %2) tern(%1:%2)
#define min(%1, %2)	(%1 < %2) tern(%1:%2)
#define	enumclass(%1)	methodmap %1 __nullable__
#define buffer(%1)	%1, sizeof(%1)
#define strbuffer(%1)	%1, sizeof(%1)
#define PLYR		MAXPLAYERS+1
#define CLNT		MAXPLAYERS+1
#define PATH		64
#define FULLPATH	PLATFORM_MAX_PATH

