#if defined _HoE_timer_included
#endinput
#endif
#define _HoE_timer_included

typedef HOE_GetBestRecordCallback = function void(int client, float time, any data);

native bool HOE_Timer_Start(int client, bool start = true);
native bool HOE_Timer_Stop(int client);

native void HOE_Timer_GetBestRecord(int client, const char[] map = "", HOE_GetBestRecordCallback callback, any data = 0);
native bool HOE_Timer_GetClientTimer(int client, int &enabled, float &time);
native void HOE_Timer_FinishRound(int client, const char[] map, float time);
native void HOE_Timer_GetCacheIDTimeString(int cache_id, const char[] sTimeString, int size);
native void HOE_Timer_GetTopRecord(char[] sName, int iSize, char[] sTimestring, int iSize2);
native void HOE_Timer_OpenTimerRecord(int client, int id);
native void HOE_Timer_ReloadCache();

native bool HOE_Timer_PluginStatus();

forward int OnFinishRound(int client, const char[] map, const char[] timeString, const char[] timeDiffString, int position, int totalrank, bool overwrite, int CourseID);

stock Timer_SecondsToTime(float seconds, char[] buffer, int maxlength, bool precision = true)
{
	int t = RoundToFloor(seconds);
	
	int hour; int mins;
	
	if (t >= 3600)
	{
		hour = RoundToFloor(t / 3600.0);
		t %= 3600;
	}
	
	if (t >= 60)
	{
		mins = RoundToFloor(t / 60.0);
		t %= 60;
	}
	
	Format(buffer, maxlength, "");

	if (hour)
	{
		Format(buffer, maxlength, "%s%02d:", buffer, hour);
	}
	
	Format(buffer, maxlength, "%s%02d:", buffer, mins);
	
	if (precision)
	{
		Format(buffer, maxlength, "%s%06.3f", buffer, float(t) + seconds - RoundToFloor(seconds));
	}
	else 
	{
		Format(buffer, maxlength, "%s%02d", buffer, t);
	}
}

stock StringToLower(char[] f_sInput)
{	
	for (int i = 0 ; i < strlen(f_sInput); i++)
	{
		f_sInput[i] = CharToLower(f_sInput[i]);
	}
}

stock Array_Copy(const any[] array, any[] newArray, int size)
{
	for (new i = 0; i < size; i++)
	{
		newArray[i] = array[i];
	}
}

stock bool String_IsNumeric(const char[] str)
{
	int x = 0;
	int dotsFound = 0;
	int numbersFound = 0;

	if (str[x] == '+' || str[x] == '-')
	{
		x++;
	}

	while (str[x] != '\0')
	{
		if (IsCharNumeric(str[x]))
		{
			numbersFound++;
		}
		else if (str[x] == '.')
		{
			dotsFound++;
			
			if (dotsFound > 1)
			{
				return false;
			}
		}
		else
		{
			return false;
		}
		
		x++;
	}
	
	if (!numbersFound)
	{
		return false;
	}
	
	return true;
}

public SharedPlugin __pl_HoE_timer = 
{
	name = "HoE-timer",
	file = "HoE-timer.smx",
#if defined REQUIRE_PLUGIN
	required = 1,
#else
	required = 0,
#endif
};

public __pl_HoE_timer_SetNTVOptional()
{
	MarkNativeAsOptional("HOE_Timer_Start");
	MarkNativeAsOptional("HOE_Timer_Stop");
	
	MarkNativeAsOptional("HOE_Timer_GetBestRecord");
	MarkNativeAsOptional("HOE_Timer_GetClientTimer");
	MarkNativeAsOptional("HOE_Timer_FinishRound");
	MarkNativeAsOptional("HOE_Timer_GetCacheIDTimeString");
	MarkNativeAsOptional("HOE_Timer_GetTopRecord");
	MarkNativeAsOptional("HOE_Timer_OpenTimerRecord");
	MarkNativeAsOptional("HOE_Timer_ReloadCache");
	
	MarkNativeAsOptional("HOE_Timer_PluginStatus");
}