#if defined _motdmgr_included
  #endinput
#endif
#define _motdmgr_included

#define VGUI_NONE 0
#define VGUI_WAIT 1
#define VGUI_READY 2
#define VGUI_ADVERT 3
#define VGUI_RADIO 4
#define VGUI_OTHER 5

#define MOTDGD (1 << 1)
#define VPP (1 << 2)

forward Action MOTDMGR_OnClientShouldShowAdvert(int client);
forward Action MOTDMGR_OnClientAccessAdvertSettings(int client);

forward int MOTDMGR_OnClientAdvertStarted(int client, int provider);
forward int MOTDMGR_OnClientAdvertFinished(int client, int provider, int time, bool abort);

forward int MOTDMGR_OnClientRadioTurnOn(int client, int channel);
forward int MOTDMGR_OnClientRadioTurnOff(int client, int channel);
forward int MOTDMGR_OnClientRadioResumed(int client, int channel);

forward int MOTDMGR_OnClientRadioChannelChanged(int client, int newvalue, int oldvalue);
forward int MOTDMGR_OnClientRadioVolumeChanged(int client, int newvalue, int oldvalue);
forward int MOTDMGR_OnClientAdvertsModeChanged(int client, int newvalue, int oldvalue);

native int MOTDMGR_GetClientRadioChannel(int client);
native int MOTDMGR_SetClientRadioChannel(int client, int channel);
native int MOTDMGR_GetClientRadioVolume(int client);
native int MOTDMGR_SetClientRadioVolume(int client, int volume);
native bool MOTDMGR_IsClientRadioEnabled(int client);
native int MOTDMGR_GetClientAdvertsMode(int client);
native int MOTDMGR_SetClientAdvertsMode(int client, int mode);

native bool MOTDMGR_IsClientAdvertPlaying(int client);
native bool MOTDMGR_AbortClientAdvert(int client);

native bool MOTDMGR_GetRadioChannelName(int channel, char name[32]);

native int MOTDMGR_StartAdvert(int client, int provider);

public SharedPlugin __pl_radio_adverts_manager = 
{
	name = "motdmgr",
	file = "motdmgr.smx",
#if defined REQUIRE_PLUGIN
	required = 1,
#else
	required = 0,
#endif
};