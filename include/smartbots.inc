#if defined _smartbots_included
  #endinput
#endif
#define _smartbots_included

enum SmartBot
{
	SmartBots_Ignore	= 0,
	SmartBots_Continue	= 1,
	SmartBots_Stop		= 2,
};

/**
 * Called right before a bot is forced to shoot after it's target was found.
 * This is where you can choose the settings such as fov, distance 
 * to change the level of the bot's attack.
 *
 * @param client Bot
 * @param target Target
 * @param visible Check visibility
 * @param sight Check FOV
 * @param flashed Check Flashed
 * @param height Check FOV height
 * @param maxDistance Max distance between client and target
 * @param fov Field of view / Angle to check within
 * @param tolerance Tolerance for the visibility TraceRay
 * @return Plugin_Continue to use default settings, Plugin_Changed for changed params and Plugin_Stop or _Handled to ignore
 */
forward SmartBot SmartBots_OnCheckPlayerPre(int client, int target, bool &visible, bool &sight, bool &flashed, bool &height, float &maxDistance, float &fov, float &tolerance)