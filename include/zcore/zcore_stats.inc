#if defined _zcore_stats_included
 #endinput
#endif
#define _zcore_stats_included

#define MAX_CONFIGS 16
#define MAX_FIELDS 32

enum ZCORE_Stats_Cache_Type {
	ZCORE_Stats_Cache_Type_Invalid = 0,
	ZCORE_Stats_Cache_Type_Int, // "int"
	ZCORE_Stats_Cache_Type_Bool,  // "bool"
	ZCORE_Stats_Cache_Type_Float,   // "float"
	ZCORE_Stats_Cache_Type_Char, // "char"
	ZCORE_Stats_Cache_Type_Auto,  // "auto"
	ZCORE_Stats_Cache_Type_MapName, // "map"
	ZCORE_Stats_Cache_Type_PlayerID, // "pid"
	ZCORE_Stats_Cache_Type_CommunityID, // "steam64"
	ZCORE_Stats_Cache_Type_Steam2ID, // "steam2"
	ZCORE_Stats_Cache_Type_Steam3ID, // "steam3"
	ZCORE_Stats_Cache_Type_HostIP, // "ip"
	ZCORE_Stats_Cache_Type_HostPort, // "port"
	ZCORE_Stats_Cache_Type_TimeStamp, // "time"
	ZCORE_Stats_Cache_Type_CurrentTime // "now"
};

native bool ZCore_Stats_GetConfigCount();

native bool ZCore_Stats_CheckConfigID	(char[] config);
native int ZCore_Stats_GetConfigID		(char[] config);
native bool ZCore_Stats_Reload			(int configID);
native bool ZCore_Stats_Save			(int configID, int index);

native bool ZCore_Stats_GetStatus		(int configID);
native bool ZCore_Stats_GetCacheStatus	(int configID);
native bool ZCore_Stats_GetFieldCount	(int configID);
native int ZCore_Stats_GetCacheCount	(int configID);

native int ZCore_Stats_GetName			(int configID, char[] output);
native int ZCore_Stats_GetFieldID		(int configID, char[] field);

native int ZCore_Stats_GetFieldType		(int configID, int fieldID);
native int ZCore_Stats_GetFieldSize		(int configID, int fieldID);
native int ZCore_Stats_GetFieldName		(int configID, int fieldID, char[] output);

native int ZCore_Stats_GetInt			(int configID, int fieldID, int index);
native bool ZCore_Stats_GetBool			(int configID, int fieldID, int index);
native float ZCore_Stats_GetFloat		(int configID, int fieldID, int index);
native bool ZCore_Stats_GetString		(int configID, int fieldID, int index, char[] output);

native bool ZCore_Stats_SetInt			(int configID, int fieldID, int index, int input);
native bool ZCore_Stats_SetBool			(int configID, int fieldID, int index, bool input);
native bool ZCore_Stats_SetFloat		(int configID, int fieldID, int index, float input);
native bool ZCore_Stats_SetString		(int configID, int fieldID, int index, char[] input, any ...);

/* Client Cache */

native bool		ZCore_Stats_GetClientCacheStatus	(int client, int configID);
native int		ZCore_Stats_GetClientIndex			(int client, int configID);
native int		ZCore_Stats_ReloadClient			(int client, int configID);
native int		ZCore_Stats_SaveClient				(int client, int configID);

native int		ZCore_Stats_GetClientInt		(int client, int configID, int fieldID);
native bool		ZCore_Stats_GetClientBool		(int client, int configID, int fieldID);
native float	ZCore_Stats_GetClientFloat		(int client, int configID, int fieldID);
native bool		ZCore_Stats_GetClientString		(int client, int configID, int fieldID, char[] output);

native bool		ZCore_Stats_SetClientInt		(int client, configID, fieldID, int input);
native bool		ZCore_Stats_SetClientBool		(int client, configID, fieldID, bool input);
native bool	ZCore_Stats_SetClientFloat		(int client, configID, fieldID, float input);
native bool		ZCore_Stats_SetClientString		(int client, configID, fieldID, char[] input, any ...);

/* Run Querys */

native int ZCore_Stats_SQL_Insert	(char[] queryKey, int configID, char[] fields, char[] values, char[] update_fields, char[] update_increase);
native int ZCore_Stats_SQL_Update	(char[] queryKey, int configID, char[] fields, char[] values, char[] update_increase);
native int ZCore_Stats_SQL_Delete	(char[] queryKey, int configID, char[] fields, char[] values, char[] checks_fields, char[] checks);

native int ZCore_Stats_SQL_Select	(char[] queryKey, int configID, char[] fields, char[] values, char[] sort_fields ,char[] checks_fields, char[] checks);

/* Callbacks */

forward bool ZCore_Stats_SQL_OnClientCreated	(int client, int configID);
forward bool ZCore_Stats_SQL_OnClientReady		(int client, int configID);
forward bool ZCore_Stats_SQL_OnClientUpdated	(int client, int configID);
forward bool ZCore_Stats_SQL_OnClientNotFound	(int client, int configID);

forward bool ZCore_Stats_SQL_OnInsertCallback	(char[] queryKey, char[] configID, char[] error);
forward bool ZCore_Stats_SQL_OnDeleteCallback	(char[] queryKey, char[] configID, char[] error);
forward bool ZCore_Stats_SQL_OnUpdateCallback	(char[] queryKey, char[] configID, char[] error);

forward bool ZCore_Stats_SQL_OnSelectCallback	(char[] queryKey, char[] configID, char[] error, Handle pack, rows); //If you don't return true it destroys the pack

public void __pl_zcore_stats_SetNTVOptional() 
{
	MarkNativeAsOptional("ZCore_Stats_GetConfigCount");
	
	MarkNativeAsOptional("ZCore_Stats_CheckConfigID");
	MarkNativeAsOptional("ZCore_Stats_GetConfigID");
	MarkNativeAsOptional("ZCore_Stats_Reload");
	MarkNativeAsOptional("ZCore_Stats_Save");
	
	MarkNativeAsOptional("ZCore_Stats_GetName");
	MarkNativeAsOptional("ZCore_Stats_GetFieldID");
	
	MarkNativeAsOptional("ZCore_Stats_GetStatus");
	MarkNativeAsOptional("ZCore_Stats_GetCacheStatus");
	MarkNativeAsOptional("ZCore_Stats_GetFieldCount");
	MarkNativeAsOptional("ZCore_Stats_GetCacheCount");
	
	MarkNativeAsOptional("ZCore_Stats_GetFieldType");
	MarkNativeAsOptional("ZCore_Stats_GetFieldSize");
	MarkNativeAsOptional("ZCore_Stats_GetFieldName");
	
	MarkNativeAsOptional("ZCore_Stats_GetInt");
	MarkNativeAsOptional("ZCore_Stats_GetBool");
	MarkNativeAsOptional("ZCore_Stats_GetFloat");
	MarkNativeAsOptional("ZCore_Stats_GetString");
	
	MarkNativeAsOptional("ZCore_Stats_SetInt");
	MarkNativeAsOptional("ZCore_Stats_SetBool");
	MarkNativeAsOptional("ZCore_Stats_SetFloat");
	MarkNativeAsOptional("ZCore_Stats_SetString");
	
	MarkNativeAsOptional("ZCore_Stats_GetClientCacheStatus");
	MarkNativeAsOptional("ZCore_Stats_GetClientIndex");
	MarkNativeAsOptional("ZCore_Stats_ReloadClient");
	MarkNativeAsOptional("ZCore_Stats_SaveClient");
	
	MarkNativeAsOptional("ZCore_Stats_GetClientInt");
	MarkNativeAsOptional("ZCore_Stats_GetClientBool");
	MarkNativeAsOptional("ZCore_Stats_GetClientFloat");
	MarkNativeAsOptional("ZCore_Stats_GetClientString");
	
	MarkNativeAsOptional("ZCore_Stats_SetClientInt");
	MarkNativeAsOptional("ZCore_Stats_SetClientBool");
	MarkNativeAsOptional("ZCore_Stats_SetClientFloat");
	MarkNativeAsOptional("ZCore_Stats_SetClientString");
	
	MarkNativeAsOptional("ZCore_Stats_SQL_Select");
	MarkNativeAsOptional("ZCore_Stats_SQL_Insert");
	MarkNativeAsOptional("ZCore_Stats_SQL_Update");
	MarkNativeAsOptional("ZCore_Stats_SQL_Delete");
}

int g_iConfigID = -1;

stock bool ZCore_Stats_Connect(char[] config)
{
	if(g_iConfigID != -1 && ZCore_Stats_CheckConfigID(config))
		g_iConfigID = ZCore_Stats_GetConfigID(config);
}

stock bool ZCore_Stats_IsConnected(char[] config)
{
	return g_iConfigID != -1;
}

