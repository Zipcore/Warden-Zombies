#if defined _zcore_playerdb_included
 #endinput
#endif
#define _zcore_playerdb_included

enum ZCore_PlayerDB_Player
{
	Player_ID,
	Player_Client,
	String:Player_Auth[32],
	String:Player_Name[64],
	Player_TimeFirstSeen,
	Player_TimeLastSeen,
	Player_TimeConnected
}

forward void ZCore_PlayerDB_OnPlayerReady(int client, int playerID, int time_total, bool created);
forward void ZCore_PlayerDB_OnPlayerQuit(int client, int playerID, int time_session, int time_total);

native int ZCore_PlayerDB_GetPlayerID(int client);
native int ZCore_PlayerDB_GetClient(int PlayerID);

native int ZCore_PlayerDB_GetName(int client, char[] buffer_64);
native int ZCore_PlayerDB_GetCommunityID(int client, char[] buffer_32);

native int ZCore_PlayerDB_GetTimeFirstSeen(int client);
native int ZCore_PlayerDB_GetTimeConnected(int client);
native int ZCore_PlayerDB_GetTimeLastSeen(int client);

native int ZCore_PlayerDB_GetNameByPlayerID(int PlayerID, char[] buffer_64);
native int ZCore_PlayerDB_GetCommunityIDByPlayerID(int PlayerID, char[] buffer_32);

native int ZCore_PlayerDB_GetTimeFirstSeenByPlayerID(int PlayerID);
native int ZCore_PlayerDB_GetTimeLastSeenByPlayerID(int PlayerID);
native int ZCore_PlayerDB_GetTimeLastDisconnectByPlayerID(int PlayerID);

public void __pl_zcore_playerdb_SetNTVOptional() 
{
	MarkNativeAsOptional("ZCore_PlayerDB_GetPlayerID");
	MarkNativeAsOptional("ZCore_PlayerDB_GetClient");
	
	MarkNativeAsOptional("ZCore_PlayerDB_GetName");
	MarkNativeAsOptional("ZCore_PlayerDB_GetCommunityID");
	MarkNativeAsOptional("ZCore_PlayerDB_GetTimeFirstSeen");
	MarkNativeAsOptional("ZCore_PlayerDB_GetTimeConnected");
	MarkNativeAsOptional("ZCore_PlayerDB_GetTimeLastSeen");
	
	MarkNativeAsOptional("ZCore_PlayerDB_GetNameByPlayerID");
	MarkNativeAsOptional("ZCore_PlayerDB_GetCommunityIDByPlayerID");
	MarkNativeAsOptional("ZCore_PlayerDB_GetTimeFirstSeenByPlayerID");
	MarkNativeAsOptional("ZCore_PlayerDB_GetTimeConnectedByPlayerID");
	MarkNativeAsOptional("ZCore_PlayerDB_GetTimeLastSeenByPlayerID");
}

stock bool GetCommunityID(char[] steamID, char[] CommunityID, int size)
{
	if(strlen(steamID) < 11 || steamID[0]!='S' || steamID[6]=='I')
	{
		CommunityID[0] = 0;
		return false;
	}
	int iUpper = 765611979;
	int iFriendID = StringToInt(steamID[10])*2 + 60265728 + steamID[8]-48;
	int iDiv = iFriendID/100000000;
	int iIdx = 9-(iDiv?iDiv/10+1:0);
	iUpper += iDiv;
	IntToString(iFriendID, CommunityID[iIdx], size-iIdx);
	iIdx = CommunityID[9];
	IntToString(iUpper, CommunityID, size);
	CommunityID[9] = iIdx;
	return true;
}