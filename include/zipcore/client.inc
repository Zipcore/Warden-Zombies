/* Loops */

#define LoopClients(%1) for(int %1 = 1; %1 <= MaxClients; %1++)

#define LoopConnectedClients(%1) for(new %1=1;%1<=MaxClients;++%1)\
								if(IsClientConnected(%1))

#define LoopIngameClients(%1) for(new %1=1;%1<=MaxClients;++%1)\
								if(IsClientInGame(%1))

#define LoopIngamePlayers(%1) for(new %1=1;%1<=MaxClients;++%1)\
								if(IsClientInGame(%1) && !IsFakeClient(%1))
								
#define LoopAuthorizedPlayers(%1) for(new %1=1;%1<=MaxClients;++%1)\
								if(IsClientConnected(%1) && IsClientAuthorized(%1))
								
#define LoopAlivePlayers(%1) for(new %1=1;%1<=MaxClients;++%1)\
								if(IsClientInGame(%1) && IsPlayerAlive(%1))
								
/* Validate */ 

stock bool Client_IsPlayer(int client)
{
	return (client > 0 && client <= MAXPLAYERS);
}

/* Client Sight */

stock Client_GetSightEnd(client, float out[3])
{
	float m_fEyes[3];
	float m_fAngles[3];
	GetClientEyePosition(client, m_fEyes);
	GetClientEyeAngles(client, m_fAngles);
	TR_TraceRayFilter(m_fEyes, m_fAngles, MASK_PLAYERSOLID, RayType_Infinite, TraceRayDontHitPlayers);
	if(TR_DidHit())
		TR_GetEndPosition(out);
}

stock void Client_LookAtPoint(int client, float point[3])
{
	float angles[3]; float clientEyes[3]; float resultant[3];
	GetClientEyePosition(client, clientEyes);
	MakeVectorFromPoints(point, clientEyes, resultant);
	GetVectorAngles(resultant, angles);
	
	if (angles[0] >= 270)
	{
		angles[0] -= 270;
		angles[0] = (90 - angles[0]);
	}
	else if (angles[0] <= 90)
		angles[0] *= -1;
	
	angles[1] -= 180;
	TeleportEntity(client, NULL_VECTOR, angles, NULL_VECTOR);
} 