
stock void Menu_CloseMenuAny(int client)
{
	Handle m_hMenu = CreateMenu(MenuHandler_CloseClientMenu);
	SetMenuTitle(m_hMenu, "Empty menu");
	DisplayMenu(m_hMenu, client, 1);
}

public MenuHandler_CloseClientMenu(Handle menu, MenuAction action, int client, int param2)
{
	if (action == MenuAction_End)
		CloseHandle(menu);
}

stock bool Menu_AddItemEx(Handle menu, int style, char[] info, char[] display[], any ...)
{
	char m_display[256];
	VFormat(m_display, sizeof(m_display), display, 5);
	return (AddMenuItem(menu, info, m_display, style)?true:false);
}

stock bool Menu_InsertItemEx(Handle menu, int position, int style, char[] info, char[] display[], any ...)
{
	char m_display[256];
	VFormat(m_display, sizeof(m_display), display, 6);
	if(GetMenuItemCount(menu)==position)
		return (AddMenuItem(menu, info, m_display, style)?true:false);
	else
		return (InsertMenuItem(menu, position, info, m_display, style)?true:false);
}

stock Panel_SetTitleEx(Handle menu, char[] display, any ...)
{
	char m_display[256];
	VFormat(m_display, sizeof(m_display), display, 3);
	SetPanelTitle(menu, m_display);
}

stock Panel_DrawItemEx(Handle menu, style, char[] display, any ...)
{
	char m_display[256];
	VFormat(m_display, sizeof(m_display), display, 4);
	return DrawPanelItem(menu, m_display, style);
}

stock int Panel_DrawPanelEx(Handle menu, char[] display, any ...)
{
	char m_display[256];
	VFormat(m_display, sizeof(m_display), display, 3);
	return DrawPanelText(menu, m_display);
}