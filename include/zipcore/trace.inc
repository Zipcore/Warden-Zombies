public bool TraceRay_DontHitSelf(int entity, int mask, any data)
{
	if(entity == data)
		return false;
	return true;
}

public bool TraceRay_DontHitPlayers(int entity, int mask, any data)
{
	if(0 < entity <= MaxClients)
		return false;
	return true;
}

public bool TraceRay_DontHitThrowerOrSelf(entity, contentsMask, any:data)
{
	int thrower = GetEntPropEnt(data, Prop_Send, "m_hThrower");
	return ((entity != data) && (entity != thrower));
}

public bool TraceRay_DontHitOwnerOrSelf(entity, contentsMask, any:data)
{
	int owner = GetEntPropEnt(data, Prop_Send, "m_hOwnerEntity");
	return ((entity != data) && (entity != owner));
}