
stock void Effect_ParentEntity(int client, int child)
{
	SetVariantString("!activator");
	AcceptEntityInput(child, "SetParent", client, child, 0);
	
	SetVariantString("IchLiebeDichPandora");
	AcceptEntityInput(child, "SetParentAttachmentMaintainOffset", child, child, 0);
}

stock void Effect_DrawBoundingBox(int client, int entity, int iSpriteIndex)
{
	float vecCenter[3];
	Entity_GetAbsOrigin(entity, vecCenter);
	
	float vecMins[3];
	GetEntPropVector(entity, Prop_Send, "m_vecMins", vecMins);
	
	float vecMaxs[3];
	GetEntPropVector(entity, Prop_Send, "m_vecMaxs", vecMaxs);
	
	float point1[3];
	point1 = vecCenter;
	float point2[3];
	point2 = vecCenter;
	
	point1[0] += vecMins[0];
	point1[1] += vecMins[1];
	point1[2] += vecMins[2];
	
	point2[0] += vecMaxs[0];
	point2[1] += vecMaxs[1];
	point2[2] += vecMaxs[2];
	
	DrawBox(client, point1, point2, 0.1, 0.2, { 255, 255, 255, 255 }, false, iSpriteIndex);
}

stock void Effect_DrawBox(int client, float fFrom[3], float fTo[3], float fLife, float width, int color[4], bool flat, int iSpriteIndex)
{
	if (!flat)
	{
		//initialize tempoary variables bottom front
		float fLeftBottomFront[3];
		fLeftBottomFront[0] = fFrom[0];
		fLeftBottomFront[1] = fFrom[1];
		fLeftBottomFront[2] = fTo[2];
		
		float fRightBottomFront[3];
		fRightBottomFront[0] = fTo[0];
		fRightBottomFront[1] = fFrom[1];
		fRightBottomFront[2] = fTo[2];
		
		//initialize tempoary variables bottom back
		float fLeftBottomBack[3];
		fLeftBottomBack[0] = fFrom[0];
		fLeftBottomBack[1] = fTo[1];
		fLeftBottomBack[2] = fTo[2];
		
		float fRightBottomBack[3];
		fRightBottomBack[0] = fTo[0];
		fRightBottomBack[1] = fTo[1];
		fRightBottomBack[2] = fTo[2];
		
		//initialize tempoary variables top front
		float fLeftTopFront[3];
		fLeftTopFront[0] = fFrom[0];
		fLeftTopFront[1] = fFrom[1];
		fLeftTopFront[2] = fFrom[2];
		
		float fRightTopFront[3];
		fRightTopFront[0] = fTo[0];
		fRightTopFront[1] = fFrom[1];
		fRightTopFront[2] = fFrom[2];
		
		//initialize tempoary variables top back
		float fLeftTopBack[3];
		fLeftTopBack[0] = fFrom[0];
		fLeftTopBack[1] = fTo[1];
		fLeftTopBack[2] = fFrom[2];
		
		float fRightTopBack[3];
		fRightTopBack[0] = fTo[0];
		fRightTopBack[1] = fTo[1];
		fRightTopBack[2] = fFrom[2];
		
		//create the box
		TE_SetupBeamPoints(fLeftTopFront, fRightTopFront, iSpriteIndex, 0, 0, 0, fLife, width, width, 10, 0.0, color, 10); (client != 0) ? TE_SendToClient(client) : TE_SendToAll();
		TE_SetupBeamPoints(fLeftTopBack, fLeftTopFront, iSpriteIndex, 0, 0, 0, fLife, width, width, 10, 0.0, color, 10); (client != 0) ? TE_SendToClient(client) : TE_SendToAll();
		TE_SetupBeamPoints(fRightTopBack, fLeftTopBack, iSpriteIndex, 0, 0, 0, fLife, width, width, 10, 0.0, color, 10); (client != 0) ? TE_SendToClient(client) : TE_SendToAll();
		TE_SetupBeamPoints(fRightTopFront, fRightTopBack, iSpriteIndex, 0, 0, 0, fLife, width, width, 10, 0.0, color, 10); (client != 0) ? TE_SendToClient(client) : TE_SendToAll();
		
		if (!flat)
		{
			TE_SetupBeamPoints(fRightBottomFront, fLeftBottomFront, iSpriteIndex, 0, 0, 0, fLife, width, width, 0, 0.0, color, 0); (client != 0) ? TE_SendToClient(client) : TE_SendToAll();
			TE_SetupBeamPoints(fLeftBottomBack, fLeftBottomFront, iSpriteIndex, 0, 0, 0, fLife, width, width, 0, 0.0, color, 0); (client != 0) ? TE_SendToClient(client) : TE_SendToAll();
			TE_SetupBeamPoints(fLeftTopFront, fLeftBottomFront, iSpriteIndex, 0, 0, 0, fLife, width, width, 0, 0.0, color, 0); (client != 0) ? TE_SendToClient(client) : TE_SendToAll();
			TE_SetupBeamPoints(fLeftBottomBack, fRightBottomBack, iSpriteIndex, 0, 0, 0, fLife, width, width, 0, 0.0, color, 0); (client != 0) ? TE_SendToClient(client) : TE_SendToAll();
			TE_SetupBeamPoints(fRightBottomFront, fRightBottomBack, iSpriteIndex, 0, 0, 0, fLife, width, width, 0, 0.0, color, 0); (client != 0) ? TE_SendToClient(client) : TE_SendToAll();
			TE_SetupBeamPoints(fRightTopBack, fRightBottomBack, iSpriteIndex, 0, 0, 0, fLife, width, width, 0, 0.0, color, 0); (client != 0) ? TE_SendToClient(client) : TE_SendToAll();
			TE_SetupBeamPoints(fRightTopFront, fRightBottomFront, iSpriteIndex, 0, 0, 0, fLife, width, width, 0, 0.0, color, 0); (client != 0) ? TE_SendToClient(client) : TE_SendToAll();
			TE_SetupBeamPoints(fLeftTopBack, fLeftBottomBack, iSpriteIndex, 0, 0, 0, fLife, width, width, 0, 0.0, color, 0); (client != 0) ? TE_SendToClient(client) : TE_SendToAll();
		}
	}
}

stock void Effect_Pentagram(float pos[3], int client = 0, float range = 200.0, float range2 = 230.0, bool trapped, bool circle, float ttl, int iSpriteIndex)
{
	float width = 3.0;
	int color[4];
	color = trapped ? {255,50,50,255} : {50,50,255,255};
	
	//Beam Ring
	TE_SetupBeamRingPoint(pos, range2, range2+0.1, gLaser1, gHalo1, 0, 10, ttl, width, 0.0, color, 10, 0);
	(client == 0) ? TE_SendToAll() : TE_SendToClient(client);
	
	//Pentagram
	float fVecStart[3];
	float fVecEnd[3];
	
	for(new i=1;i<=5;i++)
	{
		switch(i)
		{
			case 1:
			{
				Vector_GetCircuitPos(pos, range, 0.0, fVecStart, false, true);
				Vector_GetCircuitPos(pos, range, float(2*72), fVecEnd, false, true);
			}
			case 2:
			{
				Vector_GetCircuitPos(pos, range, float(2*72), fVecStart, false, true);
				Vector_GetCircuitPos(pos, range, float(4*72), fVecEnd, false, true);
			}
			case 3:
			{
				Vector_GetCircuitPos(pos, range, float(4*72), fVecStart, false, true);
				Vector_GetCircuitPos(pos, range, float(1*72), fVecEnd, false, true);
			}
			case 4:
			{
				Vector_GetCircuitPos(pos, range, float(1*72), fVecStart, false, true);
				Vector_GetCircuitPos(pos, range, float(3*72), fVecEnd, false, true);
			}
			case 5:
			{
				Vector_GetCircuitPos(pos, range, float(3*72), fVecStart, false, true);
				Vector_GetCircuitPos(pos, range, 0.0, fVecEnd, false, true);
			}
		}
		
		if(circle)
		{
			TE_SetupBeamPoints(fVecStart, fVecEnd, iSpriteIndex, 0, 0, 66, ttl, width, 1.0, 0, 0.0, color, 0);
			(client == 0) ? TE_SendToAll() : TE_SendToClient(client);
		}
	}
}

stock void Effect_CreateExplosion(float pos[3], int magnitude, int radius, int owner = 0, int team = 0)
{
	int entity = CreateEntityByName("env_explosion");
	if (entity != -1)
	{
		DispatchKeyValue(entity,"classname","hegrenade_projectile");
		
		SetEntProp(entity, Prop_Data, "m_spawnflags", 6146);
		SetEntProp(entity, Prop_Data, "m_iMagnitude", magnitude);
		SetEntProp(entity, Prop_Data, "m_iRadiusOverride", radius);
		
		DispatchSpawn(entity);
		ActivateEntity(entity);
		
		TeleportEntity(entity, pos, NULL_VECTOR, NULL_VECTOR);
		SetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity", owner);
		SetEntProp(entity, Prop_Send, "m_iTeamNum", team);
		
		EmitSoundToAll("weapons/hegrenade/explode5.wav", entity, 1, 90);
		
		AcceptEntityInput(entity, "Explode");
		DispatchKeyValue(entity,"classname","env_explosion");
		AcceptEntityInput(entity, "Kill");
	}
}