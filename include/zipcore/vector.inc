stock void Vector_AddInFrontOf(float vecOrigin[3], float vecAngle[3], float units, float output[3])
{
	float vecAngVectors[3];
	vecAngVectors = vecAngle; //Don't change input
	GetAngleVectors(vecAngVectors, vecAngVectors, NULL_VECTOR, NULL_VECTOR);
	for (int i; i < 3; i++)
	output[i] = vecOrigin[i] + (vecAngVectors[i] * units);
}

stock void Vector_GetCenter(float point1[3], float point2[3], float center[3], bool bottom)
{
	center[0] = (point1[0] + point2[0]) / 2.0;
	center[1] = (point1[1] + point2[1]) / 2.0;
	
	if(bottom)
		center[2] = point1[2];
	else
		center[2] = (point1[2] +point2[2]) / 2.0;
}

stock void Vector_GetMinMaxVec(float point1[3], float point2[3], float vecMins[3], float vecMaxs[3], bool resize)
{
	vecMins[0] = FloatAbs(point1[0] - point2[0]) / -2.0;
	vecMins[1] = FloatAbs(point1[1] - point2[1]) / -2.0;
	vecMins[2] = -1.0; // Just to be save it's not buggy
	
	vecMaxs[0] = FloatAbs(point1[0] - point2[0]) / 2.0;
	vecMaxs[1] = FloatAbs(point1[1] - point2[1]) / 2.0;
	vecMaxs[2] = FloatAbs(point1[2] - point2[2]);
	
	if(resize)
	{
		// Resize trigger (by default it's set to 16.0).  
		// A player is 32.0 unity wide, 
		// so we need to resize the trigger to have a touching border between the legs
		vecMins[0] += 16.0;
		vecMins[1] += 16.0;
		vecMins[2] += 16.0;
		
		vecMaxs[0] -= 16.0;
		vecMaxs[1] -= 16.0;
		vecMaxs[2] -= 16.0;
	}
		
	// Corrects map zones.
	float fbuffer;
	
	if (vecMins[0] > vecMaxs[0])
	{
		fbuffer = vecMins[0];
		vecMins[0] = vecMaxs[0];
		vecMaxs[0] = fbuffer;
	}
	
	if (vecMins[1] > vecMaxs[1])
	{
		fbuffer = vecMins[1];
		vecMins[1] = vecMaxs[1];
		vecMaxs[1] = fbuffer;
	}
	
	if (vecMins[2] > vecMaxs[2])
	{
		fbuffer = vecMins[2];
		vecMins[2] = vecMaxs[2];
		vecMaxs[2] = fbuffer;
	}
}

stock bool Vector_IsInsideBox(float fPCords[3], float fbsx, float fbsy, float fbsz, float fbex, float fbey, float fbez)
{
	float fpx = fPCords[0];
	float fpy = fPCords[1];
	float fpz = fPCords[2]+1;

	bool bX = false;
	bool bY = false;
	bool bZ = false;

	if (fbsx > fbex && fpx <= fbsx && fpx >= fbex)
		bX = true;
	else if (fbsx < fbex && fpx >= fbsx && fpx <= fbex)
		bX = true;

	if (fbsy > fbey && fpy <= fbsy && fpy >= fbey)
		bY = true;
	else if (fbsy < fbey && fpy >= fbsy && fpy <= fbey)
		bY = true;

	if (fbsz > fbez && fpz <= fbsz && fpz >= fbez)
		bZ = true;
	else if (fbsz < fbez && fpz >= fbsz && fpz <= fbez)
		bZ = true;

	if (bX && bY && bZ)
		return true;

	return false;
}

stock void Vector_GetCircuitPos(float center[3], float radius, float angle, float output[3], bool rotate = false, bool horizontal = false)
{
	float sin=Sine(DegToRad(angle))*radius;
	float cos=Cosine(DegToRad(angle))*radius;
	
	if(horizontal)
	{
		output[0] = center[0]+sin;
		output[1] = center[1]+cos;
		output[2] = center[2];
	}
	else
	{
		if(rotate)
		{
			output[0] = center[0]+sin;
			output[1] = center[1];
			output[2] = center[2]+cos;
		}
		else{
			output[0] = center[0];
			output[1] = center[1]+sin;
			output[2] = center[2]+cos;
		}
	}
}
