#if defined _vpp_included
 #endinput
#endif
#define _vpp_included

native void VPP_ShowAds(int client);

public SharedPlugin __pl_vpp =
{
	name = "vpp",
	file = "vpp_adverts.smx",
#if defined REQUIRE_PLUGIN
	required = 1,
#else
	required = 0,
#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_vpp_SetNTVOptional()
{
	MarkNativeAsOptional("VPP_ShowAds");
}
#endif
