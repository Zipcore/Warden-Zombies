#if defined _hunger_games_included
  #endinput
#endif
#define _hunger_games_included

enum RoundStates
{
	RS_BetweenMaps,
	RS_BetweenRounds,
	RS_DelayedRoundEnd,
	RS_FreezeTime,
	RS_InProgress,
	RS_Endgame,
	RS_Duel
};

/* Forwards */

forward void HG_OnTributeSpawn(int client);
forward void HG_OnTributeFallen(int client);

forward void HG_OnEndgameStart(float roundTime);
forward void HG_OnDuelStart(float roundTime);

forward void HG_OnRoundEnd(int winner);
forward void HG_OnDelayedRoundEnd(float delay);

// Allows to skip players in endgame checks, useful for zombies and stuff like that
// return Plugin_Stop if client is not a tribute
forward Action HG_OnCheckPlayer(int client);

/* Natives */

native RoundStates HG_GetRoundState();
native int HG_GetLastRoundWinner();
native int HG_StartDuel();