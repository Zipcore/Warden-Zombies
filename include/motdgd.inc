#if defined _motdgd_included
 #endinput
#endif
#define _motdgd_included

native void MOTDGD_ShowAds(int client);

public SharedPlugin __pl_motdgd =
{
	name = "motdgd",
	file = "motdgd.smx",
#if defined REQUIRE_PLUGIN
	required = 1,
#else
	required = 0,
#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_motdgd_SetNTVOptional()
{
	MarkNativeAsOptional("MOTDGD_ShowAds");
}
#endif
