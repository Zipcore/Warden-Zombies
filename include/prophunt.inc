#if defined _prophunt_included
  #endinput
#endif
#define _prophunt_included

/* Forwards */

forward void PH_OnHiderSpawn(int client);
forward Action PH_OnHiderTraceAttack(int client, int &attacker, int &inflictor, float &damage, int &damagetype);
forward void PH_OnHiderDeath(int client, int attacker);

forward void PH_OnSeekerSpawn(int client);
forward Action PH_OnSeekerTraceAttack(int client, int &attacker, int &inflictor, float &damage, int &damagetype);
forward void PH_OnSeekerDeath(int client, int attacker);

forward Action PH_OnHiderFreeze(int client);
forward void PH_OnHiderUnFreeze(int client);

forward Action PH_OnUpdateSpeed(int client, float &speedmul);

/* Natives */

native bool PH_IsFrozen(int client);
native bool PH_CanChangeModel(int client);
native bool PH_DisableChild(int client); //Until next spawn
native bool PH_IsChildDisabled(int client);
native bool PH_GetChildAngle(int client, int &child, float angle[3]);
native bool PH_GetModel(int client, char model[PLATFORM_MAX_PATH], char name[32], char class[32], int &skin, float &height, float angle[3]);

public void __pl_prophunt_SetNTVOptional() 
{
	MarkNativeAsOptional("PH_IsFrozen");
	MarkNativeAsOptional("PH_CanChangeModel");
	MarkNativeAsOptional("PH_DisableChild");
	MarkNativeAsOptional("PH_IsChildDisabled");
	MarkNativeAsOptional("PH_GetChildAngle");
	MarkNativeAsOptional("PH_GetModel");
}