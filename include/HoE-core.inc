#if defined _HoE_core_included
 #endinput
#endif
#define _HoE_core_included

#define MAX_QUERY_SIZE 2048
#define MAX_NAME_LENGTH_ESCAPED 2 * MAX_NAME_LENGTH + 1

native void HoE_Core_CreateClientDelay(int client);
native int HoE_Core_GetClientDelay(int client);
native bool HoE_Core_IsValidPlayer(int client, bool bIsAlive = false, bool bIsFakeClient = false);
native bool HoE_Core_HasFlags(int client, const char[] sFlags);

native void HoE_Core_FetchPrefix(char[] sPrefix, int size);
native void HoE_Core_Log(const char[] log, any ...);
native void HoE_Core_DatabaseEntry(char[] entry, int size);

//Stocks
stock void PushMenuCell(Handle hndl, const char[] id, int data)
{
	char DataString[64];
	IntToString(data, DataString, sizeof(DataString));
	AddMenuItem(hndl, id, DataString, ITEMDRAW_IGNORE);
}

stock int GetMenuCell(Handle hndl, const char[] id, int DefaultValue = 0)
{
	int ItemCount = GetMenuItemCount(hndl);
	char info[64]; char data[64];
	
	for (int i = 0; i < ItemCount; i++)
	{
		GetMenuItem(hndl, i, info, sizeof(info), _, data, sizeof(data));
		
		if (StrEqual(info, id))
		{
			return StringToInt(data);
		}
	}
	
	return DefaultValue;
}

stock bool AddMenuItemFormat(Handle &menu, const char[] info, int style = ITEMDRAW_DEFAULT, const char[] format, any...)
{
	char display[128];
	VFormat(display, sizeof(display), format, 5);
	
	return AddMenuItem(menu, info, display, style);
}

stock void PushMenuString(Handle hndl, const char[] id, const char[] data)
{
	AddMenuItem(hndl, id, data, ITEMDRAW_IGNORE);
}

stock bool GetMenuString(Handle hndl, const char[] id, char[] Buffer, int size)
{
	int ItemCount = GetMenuItemCount(hndl);
	char info[64]; char data[64];
	
	for (int i = 0; i < ItemCount; i++)
	{
		GetMenuItem(hndl, i, info, sizeof(info), _, data, sizeof(data));
		
		if (StrEqual(info, id))
		{
			strcopy(Buffer, size, data);
			return true;
		}
	}
	
	return false;
}
//

#if !defined REQUIRE_PLUGIN
public __pl_HoE_core_SetNTVOptional()
{
	MarkNativeAsOptional("HoE_Core_CreateClientDelay");
	MarkNativeAsOptional("HoE_Core_GetClientDelay");
	MarkNativeAsOptional("HoE_Core_IsValidPlayer");
	MarkNativeAsOptional("HoE_Core_HasFlags");
	
	MarkNativeAsOptional("HoE_Core_FetchPrefix");
	MarkNativeAsOptional("HoE_Core_Log");
	MarkNativeAsOptional("HoE_Core_DatabaseEntry");
}
#endif

public SharedPlugin __pl_HoE_core =
{
	name = "HoE-core",
	file = "HoE-core.smx",
	#if defined REQUIRE_PLUGIN
	required = 1,
	#else
	required = 0,
	#endif
};
