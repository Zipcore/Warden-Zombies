#if defined _rankings_included
 #endinput
#endif
#define _rankings_included

/* Forwards */

/**
 * Called when a player receives points.
 * @param client
 * @param points
 * @param added
 *
 * @noreturn
 */
forward Rankings_OnPlayerGainPoints(client, points, added);  

/**
 * Called when a player loses points.
 * @param client
 * @param points
 * @param removed
 *
 * @noreturn
 */
forward Rankings_OnPlayerLostPoints(client, points, removed);  

/**
 * Called when an admin sets a player's points.
 * @param client
 * @param points
 * @param removed
 *
 * @noreturn
 */
forward Rankings_OnPlayerSetPoints(client, points, old);  

/**
 * Called when a player's points are loaded from the database.
 * @param client
 * @param points
 * @param removed
 *
 * @noreturn
 */
forward Rankings_OnPlayerPointsLoaded(client, points);  

/**
 * Called when a player's rank are loaded from the database.
 * @param client
 * @param points
 * @param removed
 *
 * @noreturn
 */
forward Rankings_OnPlayerRankLoaded(client, rank);  

/* Natives */

/**
 * Returns the player's current points.
 * @param client
 *
 * @return Points
 */
native Rankings_GetPoints(client);

/**
 * Returns the player's current rank in the server.
 * @param client
 *
 * @return rank
 */
native Rankings_GetPointRank(client);

/**
 * Stores the player's tag in the buffer provided.
 * @param buffer
 * @param maxlen
 * @param client
 *
 * @noreturn
 */
native Rankings_GetTag(String:tag[], size, client);

/**
 * Stores the player's tag including the defined chat colors in the buffer provided.
 * @param buffer
 * @param maxlen
 * @param client
 *
 * @noreturn
 */
native Rankings_GetChatTag(String:tag[], size, client);

/**
 * Sets the player's points to the number defined.
 * @param client
 * @param points
 *
 * @noreturn
 */
native Rankings_SetPoints(client, points);

/**
 * Adds the number defined to the player's total points.
 * @param client
 * @param points
 *
 * @noreturn
 */
native Rankings_AddPoints(client, points);

/**
 * Decreases a player's points by the number defined.
 * @param client
 * @param points
 *
 * @noreturn
 */
native Rankings_RemovePoints(client, points);

/**
 * Saves the player's points in the database.
 * @param client
 *
 * @noreturn
 */
native Rankings_SavePoints(client);

/**
 * Refreshes a player's points from the database.
 * @param client
 *
 * @noreturn
 */
native Rankings_RefreshPoints(client);

/**
 * Refreshes all players' points from the database.
 *
 * @noreturn
 */
native Rankings_RefreshPointsAll();
	
public SharedPlugin:__pl_rankings = 
{
	name = "rankings",
	file = "rankings.smx",
#if defined REQUIRE_PLUGIN
	required = 1,
#else
	required = 0,
#endif
};

public __pl_rankings_SetNTVOptional() 
{
	MarkNativeAsOptional("Rankings_GetPoints");
	MarkNativeAsOptional("Rankings_GetPointRank");
	MarkNativeAsOptional("Rankings_GetTag");
	MarkNativeAsOptional("Rankings_GetChatTag");
	MarkNativeAsOptional("Rankings_SavePoints");
	MarkNativeAsOptional("Rankings_AddPoints");
	MarkNativeAsOptional("Rankings_SetPoints");
	MarkNativeAsOptional("Rankings_RemovePoints");
	MarkNativeAsOptional("Rankings_RefreshPoints");
	MarkNativeAsOptional("Rankings_RefreshPointsAll");
}