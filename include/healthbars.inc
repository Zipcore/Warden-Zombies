#if defined _healthbars_included
 #endinput
#endif
#define _healthbars_included

native int Healthbar_Remove(int client);
native int Healthbar_Update(int client);
native int Healthbar_Set(int client, float zpos, int maxhp, char[] sprite_pack, float scale);
native int Healthbar_AddPack(char[] sprite_pack);

public void __pl_healthbars_SetNTVOptional() 
{
	MarkNativeAsOptional("Healthbar_Reset");
	MarkNativeAsOptional("Healthbar_Update");
	MarkNativeAsOptional("Healthbar_Set");
	MarkNativeAsOptional("Healthbar_AddPack");
}