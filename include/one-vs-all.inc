#if defined _one_vs_all_included
  #endinput
#endif
#define _one_vs_all_included

native int OVA_IsBoss(int client);
native int OVA_GetBoss();
native int OVA_RegisterBoss(char name[32], int chance, int team);

forward void OVA_OnBossInit(int client, char name[32]);