#if defined _warden_included
 #endinput
#endif
#define _warden_included

native bool IsPlayerWarden(int client);
forward OnWardenCreated(int client);
forward OnWardenRemoved(int client);

public SharedPlugin __pl_warden =
{
	name = "warden",
	file = "warden.smx",
#if defined REQUIRED_PLUGIN
	required = 1,
#else
	required = 0,
#endif
};

#if !defined REQUIRED_PLUGIN
public __pl_warden_SetNTVOptional()
{
	MarkNativeAsOptional("IsPlayerWarden");
}
