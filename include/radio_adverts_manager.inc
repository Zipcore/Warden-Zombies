#if defined _radio_adverts_manager_included
  #endinput
#endif
#define _radio_adverts_manager_included

#define MOTDGD (1 << 1)
#define VPP (1 << 2)
#define PINION (1 << 3)

forward Action RAM_OnClientShouldShowAdvert(int client);
forward Action RAM_OnClientAccessAdvertSettings(int client);

forward int RAM_OnClientAdvertStarted(int client, int provider);
forward int RAM_OnClientAdvertFinished(int client, int provider, int time, bool abort);

forward int RAM_OnClientRadioTurnOn(int client, int channel);
forward int RAM_OnClientRadioTurnOff(int client, int channel);
forward int RAM_OnClientRadioResumed(int client, int channel);

forward int RAM_OnClientRadioChannelChanged(int client, int newvalue, int oldvalue);
forward int RAM_OnClientRadioVolumeChanged(int client, int newvalue, int oldvalue);
forward int RAM_OnClientAdvertsModeChanged(int client, int newvalue, int oldvalue);

native int RAM_GetClientRadioChannel(int client);
native int RAM_SetClientRadioChannel(int client, int channel);
native int RAM_GetClientRadioVolume(int client);
native int RAM_SetClientRadioVolume(int client, int volume);
native bool RAM_IsClientRadioEnabled(int client);
native int RAM_GetClientAdvertsMode(int client);
native int RAM_SetClientAdvertsMode(int client, int mode);

native bool RAM_IsClientAdvertPlaying(int client);
native bool RAM_AbortClientAdvert(int client);

native bool RAM_GetRadioChannelName(int channel, char name[32]);

native int RAM_StartAdvert(int client, int provider);

public SharedPlugin __pl_radio_adverts_manager = 
{
	name = "radio_adverts_manager",
	file = "radio_adverts_manager.smx",
#if defined REQUIRE_PLUGIN
	required = 1,
#else
	required = 0,
#endif
};