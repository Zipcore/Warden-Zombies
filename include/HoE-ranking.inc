#if defined _HoE_ranking_included
#endinput
#endif
#define _HoE_ranking_included

typedef HOE_GetRanksCallback = function void(int id, int id_map, int rank, int total, int rank_map, int total_map, any data);
typedef HOE_GetIDFromRankCallback = function void(int id, int rank, any data);

native void HOE_Ranking_OpenPrivateRecords(int client, int id);
native int HOE_Ranking_SteamIDToDatabaseID(int SteamID);
native void HOE_Ranking_GetClientRankings(int id, HOE_GetRanksCallback callback, any data);
native void HOE_Ranking_GetIDFromRank(int rank, HOE_GetIDFromRankCallback callback, any data);
native int HOE_Ranking_GetClientID(int client);

native int HOE_Ranking_GetClientPoints(int client);
native void HOE_Ranking_AddClientPoints(int client, int points = 1, bool bIsZone = false);

#if defined REQUIRE_PLUGIN
public __pl_HoE_ranking_SetNTVOptional()
{	
	MarkNativeAsOptional("HOE_Ranking_OpenPrivateRecords");
	MarkNativeAsOptional("HOE_Ranking_SteamIDToDatabaseID");
	MarkNativeAsOptional("HOE_Ranking_GetClientRankings");
	MarkNativeAsOptional("HOE_Ranking_GetIDFromRank");
	MarkNativeAsOptional("HOE_Ranking_GetClientID");
	
	MarkNativeAsOptional("HOE_Ranking_GetClientPoints");
	MarkNativeAsOptional("HOE_Ranking_AddClientPoints");
}
#endif

public SharedPlugin __pl_HoE_ranking =
{
	name = "HoE-ranking",
	file = "HoE-ranking.smx",
#if defined REQUIRE_PLUGIN
	required = 1,
#else
	required = 0,
#endif
};
