#if defined _wardenmarkers_included
  #endinput
#endif
#define _wardenmarkers_included

/* Forwards */

forward void WardenMarkers_OnDisplayMarkerOptions(Handle menu);
forward void WardenMarkers_OnMarkerThink(float pos[3], float radius, char[] options);

/* Natives */

native int WardenMarkers_RegisterOption(char[] option);
native int WardenMarkers_RemoveOption(char[] option);

public void __pl_zcore_hungergames_SetNTVOptional() 
{
	MarkNativeAsOptional("WardenMarkers_RegisterOption");
	MarkNativeAsOptional("WardenMarkers_RemoveOption");
}