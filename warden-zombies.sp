#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "Warden - Zombies"
#define PLUGIN_VERSION "1.0"
#define PLUGIN_DESCRIPTION ""
#define PLUGIN_URL "zipcore.net"

public Plugin myinfo = 
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
}

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>
#include <smlib>
#include <multicolors>
#include <warden>

#define LoopClients(%1) for(int %1 = 1; %1 <= MaxClients; %1++)

#define LoopIngameClients(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1))

#define LoopAlivePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && IsPlayerAlive(%1))

bool g_bZombieMode;
int g_iBlockedRounds;

int g_iBombRing;
int g_iHalo;

#define VMT_BOMBRING "materials/sprites/bomb_planted_ring.vmt"
#define VMT_HALO "materials/sprites/halo.vmt"

ConVar g_cvBeaconLast;
int g_iBeaconLast;

ConVar g_cvRoundLimit;
int g_iRoundLimit;

ConVar g_cvSlow;
float g_fSlow;

public void OnPluginStart()
{
	CreateConVar("warden_zombies_version", PLUGIN_VERSION, "Warden - Zombies version", FCVAR_DONTRECORD|FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
	
	g_cvRoundLimit = CreateConVar("warden_zombies_round_limit", "5", "Block zombie freeday for this amount of rounds.");
	g_iRoundLimit = GetConVarInt(g_cvRoundLimit);
	HookConVarChange(g_cvRoundLimit, OnSettingChanged);
	
	g_cvBeaconLast = CreateConVar("warden_zombies_beacon_last", "5", "If there are equal or less player alive beacon all alive players.");
	g_iBeaconLast = GetConVarInt(g_cvBeaconLast);
	HookConVarChange(g_cvBeaconLast, OnSettingChanged);
	
	g_cvSlow = CreateConVar("warden_zombies_slow", "0.5", "Speed for zombies (0.5 = 50%).");
	g_fSlow = GetConVarFloat(g_cvSlow);
	HookConVarChange(g_cvSlow, OnSettingChanged);
	
	AutoExecConfig(true, "warden-zombies");
	
	LoopIngameClients(client)
		OnClientPutInServer(client);
	
	HookEvent("round_end", Event_RoundEnd);
	
	RegConsoleCmd("sm_zombie", Cmd_Zombies);
}

public int OnSettingChanged(Handle convar, const char[] oldValue, const char[] newValue)
{
	if(convar == g_cvRoundLimit)
		g_iRoundLimit = StringToInt(newValue);
	else if (convar == g_cvBeaconLast)
		g_iBeaconLast = StringToInt(newValue);
	else if (convar == g_cvSlow)
		g_fSlow = StringToFloat(newValue);
}

public void OnMapStart()
{
	g_iBombRing = PrecacheModel(VMT_BOMBRING);
	g_iHalo = PrecacheModel(VMT_HALO);
	
	CreateTimer(1.0, Timer_Beacon, _, TIMER_REPEAT | TIMER_FLAG_NO_MAPCHANGE);
	
	g_iBlockedRounds = 0;
	
	g_bZombieMode = false;
}

public void OnClientPutInServer(int client)
{
	SDKHook(client, SDKHook_WeaponCanUse, OnWeaponCanUse);
}

public void OnClientDisconnect(int client)
{
	SDKUnhook(client, SDKHook_WeaponCanUse, OnWeaponCanUse);
}

public Action Event_RoundEnd(Handle event, const char[] name, bool dontBroadcast)
{
	if(g_iBlockedRounds > 0)
		g_iBlockedRounds--;
	
	if(g_bZombieMode)
	{
		ServerCommand("mp_teammates_are_enemies 0");
	}
	
	g_bZombieMode = false;
	
	return Plugin_Continue;
}

public Action Cmd_Zombies(int client, int args)
{
	if(!warden_iswarden(client))
	{
		CPrintToChat(client, "{darkred}Only the warden can use this command!");
		return Plugin_Handled;
	}
	
	if(g_iBlockedRounds > 0)
	{
		CPrintToChat(client, "{darkred}You have to wait %d round to use this again!", g_iBlockedRounds);
		return Plugin_Handled;
	}
	
	CPrintToChat(client, "{darkred}Zombie Freeday started!", g_iBlockedRounds);
	
	ServerCommand("mp_teammates_are_enemies 1");
	
	g_bZombieMode = true;
	
	LoopAlivePlayers(i)
	{
		StripPlayerWeapons(i, true, true);
		SetEntPropFloat(i, Prop_Data, "m_flLaggedMovementValue", g_fSlow);
	}
	
	g_iBlockedRounds = g_iRoundLimit;
	
	return Plugin_Handled;
}

public Action OnWeaponCanUse(int client, int weapon)
{
	if(!g_bZombieMode)
		return Plugin_Continue;
	
	if(GetEntProp(weapon, Prop_Send, "m_iItemDefinitionIndex") == 31)
		return Plugin_Handled;
	
	char sWeapon[32];
	GetEntityClassname(weapon, sWeapon, sizeof(sWeapon));
	
	if(StrContains(sWeapon, "knife", false) != -1 || StrContains(sWeapon, "bayonet") != -1)
		return Plugin_Continue;
	
	return Plugin_Handled;
}

public Action Timer_Beacon(Handle timer, any data)
{
	CreateBeacons();
	return Plugin_Continue;
}

void CreateBeacons()
{
	if(!g_bZombieMode)
		return;
	
	int iAlive;
	LoopAlivePlayers(i)
	{
		SetEntPropFloat(i, Prop_Data, "m_flLaggedMovementValue", g_fSlow);
		iAlive++;
	}
	
	if(iAlive > g_iBeaconLast)
		return;
	
	LoopAlivePlayers(i)
	{
		float fPos[3];
		GetClientAbsOrigin(i, fPos);
		fPos[2] += 8;
		
		TE_SetupBeamRingPoint(fPos, 10.0, 750.0, g_iBombRing, g_iHalo, 0, 10, 0.6, 10.0, 0.5, {255, 75, 75, 255}, 5, 0);
		TE_SendToAll();
	}
}

void StripPlayerWeapons(int client, bool remove, bool keepknife = false)
{
	int iWeapon = -1;
	for (int i = CS_SLOT_PRIMARY; i <= CS_SLOT_C4; i++)
	{
		if(keepknife && i == CS_SLOT_KNIFE)
			continue;
		
		while((iWeapon = GetPlayerWeaponSlot(client, i)) != -1)
		{
			if(remove)
				RemovePlayerItem(client, iWeapon);
			else CS_DropWeapon(client, iWeapon, false);
		}
	}
}